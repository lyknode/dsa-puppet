# salsa/gitlab users
class salsa::users inherits salsa {
  group { '_salsa':
    gid => $local_uid_base,
  }

  file { '/srv/salsa-users':
    ensure => directory,
  }

  Integer[0,19].each |$i| {
    $user = sprintf('_salsa%02d', $i)
    $home = sprintf('/srv/salsa-users/salsa%02d', $i)
    user { $user:
      uid        => $local_uid_base + $i,
      gid        => '_salsa',
      home       => $home,
      shell      => '/bin/sh',
      managehome => true,
      require    => File['/srv/salsa-users'],
    }
  }
}
