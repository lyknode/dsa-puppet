# This class is included by all the *master services
# that run dak, like ftp-master and security-master.
class roles::dakmaster {
  apache2::config { 'puppet-builddlist':
    content => template('roles/dakmaster/conf-builddlist.erb'),
  }
}
