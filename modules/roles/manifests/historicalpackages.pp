class roles::historicalpackages {
  include apache2
  ssl::service { 'historical.packages.debian.org': notify  => Exec['service apache2 reload'], key => true, }
}
