# udd parameters
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::udd::params (
  String  $db_address = $roles::udd::db_address,
  Integer $db_port    = $roles::udd::db_port,
) {
}
