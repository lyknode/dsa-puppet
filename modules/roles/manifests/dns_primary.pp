# the primary (hidden master) nameserver does bind zone file stuff and letsencrypt cert handling
#
# it will not, by default, open the firewall for requests.  however, it will
# collect ferm simple rules tagged named::primary::ferm which our own
# secondaries (the geo nodes) and the monitoring infrastructure export.
# Additional networks can be set with allow_access for any 3rd party nodes that
# should have access.
# @param allow_access additional hosts/network that should be allowed to port 53
class roles::dns_primary(
  Array[Stdlib::IP::Address] $allow_access = [],
) {
  include named::primary

  # ssh setup to sync the geonodes
  ssh::authorized_key_collect { 'dns_primary-dnsadm':
    target_user => 'dnsadm',
    collect_tag => 'dns_primary',
  }
  ssh::authorized_key_collect { 'dns_primary-letsencrypt':
    target_user => 'letsencrypt',
    collect_tag => 'dns_primary',
  }
  ssh::authorized_key_collect { 'dns_primary-geodnssync':
    target_user => 'geodnssync',
    collect_tag => 'dns_primary',
  }

  ssh::keygen {'dnsadm': }
  ssh::authorized_key_add { 'dns_primary::geodns':
    target_user => 'geodnssync',
    command     => '/etc/bind/geodns/trigger',
    key         => $facts['dnsadm_key'],
    collect_tag => 'geodnssync-node',
  }

  # ssh setup to sync letsencrypt info to puppet
  ssh::keygen {'letsencrypt': }
  ssh::authorized_key_add { 'dns_primary::puppetmaster::letsencrypt-certificates':
    target_user => 'puppet',
    command     => 'rsync --server -vlogDtprze.iLsfx --delete --partial . /srv/puppet.debian.org/from-letsencrypt',
    key         => $facts['letsencrypt_key'],
    collect_tag => 'puppetmaster',
  }

  # firewalling
  ferm::rule::simple { 'dns-from-secondaries':
    description => 'Allow additional (such as 3rd party secondary nameserver) access to the primary',
    proto       => ['udp', 'tcp'],
    port        => 'domain',
    saddr       => $allow_access,
  }
  Ferm::Rule::Simple <<| tag == 'named::primary::ferm' |>>

  # mini-nag does nrpe queries to check if hosts are still up
  @@ferm::rule::simple { "dsa-nrpe-from-dnsprimary-${::fqdn}":
    tag         => 'nagios-nrpe::server',
    description => 'Allow dns primary running mini-nag access to the nrpe daemon',
    port        => '5666',
    saddr       => $base::public_addresses,
  }
  @@concat::fragment { "nrpe-debian-allow-${::fqdn}":
    tag     => 'nagios-nrpe::server::debianorg.cfg',
    target  => '/etc/nagios/nrpe.d/debianorg.cfg',
    content => "allowed_hosts=${ $base::public_addresses.join(', ') }",
  }

  concat::fragment { 'puppet-crontab--nagios--dsa-check-zone-rrsig-expirations':
    target  => '/etc/cron.d/puppet-crontab',
    order   => '010',
    content => @(EOF)
      */20 * * * * root dsa-wrap-nagios-check -s dns-check-zone-rrsig-expirations dsa-check-zone-rrsig-expiration-many --exclude-ns d.in-addr-servers.arpa,d.ip6-servers.arpa --warn 20d --critical 7d --geozonedir /srv/dns.debian.org/repositories/auto-dns/zones /srv/dns.debian.org/repositories/domains
      | EOF
  }

  ensure_packages ( [ 'python3-dnspython', 'python3-yaml', 'python3-pycryptodome', 'awscli' ] )
}
