# this is pulled in by *-mirror or syncproxy roles
# in ensures the archvsync user has a home, and
# that mirrormaster can ssh to it
class roles::archvsync_base {
  file { '/srv/mirrors':
    ensure => directory,
    owner  => root,
    group  => 'archvsync',
    mode   => '0775',
  }

  file { '/srv/mirrors/.nobackup':
    ensure  => present,
    content => '',
  }

  file { '/etc/ssh/userkeys/archvsync':
    ensure => 'link',
    target => '/home/archvsync/.ssh/authorized_keys',
  }

  Ferm::Rule::Simple <<| tag == 'ssh::server::to::archvsync' |>>

  file { '/usr/local/bin/archvsync-run-all':
    source => 'puppet:///modules/roles/archvsync_base/archvsync-run-all',
    mode   => '0555',
  }
  concat::fragment { 'puppet-crontab--archvsync':
    target  => '/etc/cron.d/puppet-crontab',
    content => @("EOF"),
        @reboot archvsync sleep 60; chronic archvsync-run-all
        | EOF
  }
}
