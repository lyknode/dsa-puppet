# wanna-build parameters
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::buildd_master::params (
  String  $db_address = $roles::buildd_master::db_address,
  Integer $db_port    = $roles::buildd_master::db_port,
) {
}
