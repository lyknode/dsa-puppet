# pybuildd configuration
class buildd::pybuildd {
  package { 'buildd':
    ensure => purged,
  }

  package { ['python3-retrying', 'python3-yaml']:
    ensure => installed,
  }
  file { '/home/buildd/.profile':
    content  => @(EOT),
      export XDG_RUNTIME_DIR="/run/user/$(id -u)"
      export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"
      | EOT
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/.forward':
    content  => ":blackhole:\n",
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/logs':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
  dsa_systemd::linger { 'buildd': }
  file { '/etc/systemd/journald.conf.d':
    ensure  => directory,
    mode    => '755',
  }
  file { '/etc/systemd/journald.conf.d/persistency.conf':
    source => 'puppet:///modules/dsa_systemd/persistency.conf',
  }

  # Make sure that the build directory have the correct permissions.
  # This should go away once pybuildd issue #3 is solved.
  file { '/home/buildd/build':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
  # work around https://salsa.debian.org/wb-team/pybuildd/issues/11
  concat::fragment { 'puppet-crontab--pybuildd-expire-logs':
    target => '/etc/cron.d/puppet-crontab',
    content  => @(EOF)
      @daily buildd [ -d ~buildd/logs ] && find ~buildd/logs -type f -mtime +90 -delete
      | EOF
  }
}
