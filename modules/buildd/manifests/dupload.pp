# dupload configuration
class buildd::dupload {
  package { 'dupload':
    ensure => installed,
  }
  file { '/etc/dupload.conf':
    content  => template('buildd/dupload.conf.erb'),
    require => Package['dupload'],
  }
}
