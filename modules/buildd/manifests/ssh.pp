# SSH configuration
class buildd::ssh {
  ssh::keygen {'buildd': }

  ssh::authorized_key_add { 'buildd::buildd_master':
    target_user => 'wb-buildd',
    command     => "/srv/wanna-build/bin/wanna-build --ssh-wrapper ${::hostname}",
    key         => $facts['buildd_key'],
    collect_tag => 'buildd_master',
  }

  ssh::authorized_key_add { 'buildd::buildd_upload':
    target_user => 'buildd-uploader',
    command     => "/home/buildd-uploader/rsync-ssh-wrap ${::hostname}",
    key         => $facts['buildd_key'],
    collect_tag => 'buildd_upload',
  }
}
