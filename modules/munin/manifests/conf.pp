# a conf.d snippet
# @param ensure check enabled/disabled
# @param content content to put into plugin-conf.d/<name>
# @param source file to put into plugin-conf.d/<name>
define munin::conf (
  Enum['present','absent'] $ensure = 'present',
  Optional[String] $content = undef,
  Optional[String] $source = undef,
) {
  include munin

  file { "/etc/munin/plugin-conf.d/${name}":
    ensure  => $ensure,
    source  => $source,
    content => $content,
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }
}
