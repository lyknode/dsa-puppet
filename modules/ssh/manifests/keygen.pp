# create an ssh key for user
define ssh::keygen(
  String $user = $name,
  String $keyfile = 'id_rsa',
) {
  if ! $facts['ssh_keys_users'] {
    notify { 'We do not have an ssh_keys_users fact!':
      loglevel => warning,
    }
  } elsif ! $facts['ssh_keys_users'][$user] or ! $facts['ssh_keys_users'][$user]["${keyfile}.pub"] {
    # We use su - rather than user => so that our pam config creates the homedir
    # for the user if it does not yet exist.
    exec { "create-${user}-ssh-key-${keyfile}":
      command => @("EOF"),
        /bin/su - ${user} -c 'mkdir -p -m 02700 .ssh && ssh-keygen -C "`whoami`@`hostname` (`date +%Y-%m-%d`)" -P "" -f .ssh/${keyfile} -q'
        | EOF
      onlyif  => "/usr/bin/getent passwd '${user}' > /dev/null && ! [ -e ~${user}/'.ssh/${keyfile}' ]",
    }
  }
}
