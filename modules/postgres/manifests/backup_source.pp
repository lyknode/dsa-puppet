#
# See dsa-wiki input/howto/postgres-backup.creole for some documentation
#
class postgres::backup_source {
  include postgres::backup_server::register_backup_clienthost

  file { '/usr/local/bin/pg-backup-file':
    mode   => '0555',
    source => 'puppet:///modules/postgres/backup_source/pg-backup-file',
  }
  file { '/etc/dsa/pg-backup-file.conf':
    content => template('postgres/backup_source/pg-backup-file.conf.erb'),
  }

  file { '/usr/local/bin/pg-receive-file-from-backup':
    mode   => '0555',
    source => 'puppet:///modules/postgres/pg-receive-file-from-backup',
  }

  ssh::keygen {'postgres': }
}
