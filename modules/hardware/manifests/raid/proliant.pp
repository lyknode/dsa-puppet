# Set up raid and health monitoring for HPE ProLiant servers
class hardware::raid::proliant {
  if $::smartarraycontroller_hpsa or $::smartarraycontroller_cciss {
    include debian_org::apt_restricted

    package { 'hpacucli':
      ensure => installed,
      tag    => extra_repo,
    }
    package { 'hpssacli':
      ensure => installed,
      tag    => extra_repo,
    }
    if !($::systemproductname in ['ProLiant DL180 G5', 'ProLiant DL120 G5', 'ProLiant ML150 G5']) {
      package { 'hp-health':
        ensure => installed,
        tag    => extra_repo,
      }
    }

    if $::debarchitecture == 'amd64' {
      package { 'lib32gcc1':
        ensure => installed,
      }
    }

    if $::smartarraycontroller_hpsa {
      $cmd = $::hostname ? {
        'lobos'    => 'dsa-check-hpssacli --no-battery',
        'villa'    => 'dsa-check-hpssacli --no-battery',
        'lw08'     => 'dsa-check-hpssacli --no-battery --ignore-cache',
        'wieck'    => 'dsa-check-hpssacli --ignore-cache',
        'schumann' => 'dsa-check-hpssacli --ignore-cache',
        default    => 'dsa-check-hpssacli'
      }

      concat::fragment { 'puppet-crontab--nagios--dsa-check-hpssacli':
        target  => '/etc/cron.d/puppet-crontab',
        order   => '020',
        content => @("EOF")
          27 */2 * * * root sleep $(( RANDOM \% 900 )); dsa-wrap-nagios-check ${cmd}
          | EOF
      }
    }
    file { '/etc/cron.d/puppet-nagios-hpsa': ensure => absent, }
  }
}
